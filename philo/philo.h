/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 12:05:36 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/15 11:05:04 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <sys/time.h>
# include <unistd.h>
# include <stdio.h>
# include <pthread.h>
# include <limits.h>
# include <stdlib.h>
# include <stdarg.h>

typedef struct s_phi{
	int				i;
	int				nb_eat;
	int				t_die;
	int				t_eat;
	int				t_sleep;
	int				nb_philo;
	long			l_eat;
	pthread_t		thread;
	pthread_mutex_t	eat_m;
	pthread_mutex_t	*l_fork;
	pthread_mutex_t	*r_fork;
	struct s_env	*env;
}	t_phi;

typedef struct s_env{
	int				dead;
	int				nb_philo;
	int				t_die;
	int				t_eat;
	int				t_sleep;
	long			max_eat;
	long			t_start;
	t_phi			*phi;
	pthread_t		checker;
	pthread_mutex_t	*fork;
	pthread_mutex_t	write_m;
	pthread_mutex_t	dead_m;
}	t_env;

int		error_args(void);
int		min(int a, int b);
int		max(int a, int b);
int		s_atoi(char *str);
int		init_env(t_env *e);
int		error_threads(void);
int		is_number(char *str);
int		someone_dead(t_env *e);
int		check_death_and_meal(t_env *e);
int		ft_free(int return_val, int nb_free, ...);

long	gettime(void);

void	philo_sleep(int ms);
void	*routine(void *ptr);
void	lock(pthread_mutex_t *m);
void	unlock(pthread_mutex_t *m);
void	print(t_phi *phi, char *str);

#endif
