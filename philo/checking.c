/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checking.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 15:12:47 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/17 16:12:08 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	someone_dead(t_env *e)
{
	int	dead;

	lock(&e->dead_m);
	dead = e->dead;
	unlock(&e->dead_m);
	return (dead);
}

long	get_last_eat(t_phi *phi)
{
	long	l_eat;

	lock(&phi->eat_m);
	l_eat = phi->l_eat;
	unlock(&phi->eat_m);
	return (l_eat);
}

long	get_nb_eat(t_phi *phi)
{
	long	nb_eat;

	lock(&phi->eat_m);
	nb_eat = phi->nb_eat;
	unlock(&phi->eat_m);
	return (nb_eat);
}

int	check_meal(t_env *e, int nb_eat)
{
	if (nb_eat == e->max_eat)
	{
		lock(&e->dead_m);
		e->dead = 1;
		unlock(&e->dead_m);
		return (1);
	}
	return (0);
}

int	check_death_and_meal(t_env *e)
{
	int	i;
	int	nb_eat_min;

	nb_eat_min = INT_MAX;
	i = -1;
	while (++i < e->nb_philo && !someone_dead(e))
	{
		if (gettime() - get_last_eat(&e->phi[i]) > e->phi[i].t_die)
		{
			lock(&e->dead_m);
			e->dead = 1;
			unlock(&e->dead_m);
			lock(&e->write_m);
			printf("%ld %d died\n", gettime() - e->t_start, i + 1);
			unlock(&e->write_m);
			return (1);
		}
		nb_eat_min = min (nb_eat_min, get_nb_eat(&e->phi[i]));
	}
	return (check_meal(e, nb_eat_min));
}
