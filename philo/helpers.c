/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 15:14:46 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/17 16:18:04 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

long	gettime(void)
{
	struct timeval	now;
	long			ms;

	gettimeofday(&now, NULL);
	ms = now.tv_sec * 1000 + now.tv_usec / 1000;
	return (ms);
}

void	lock(pthread_mutex_t *m)
{
	pthread_mutex_lock(m);
}

void	unlock(pthread_mutex_t *m)
{
	pthread_mutex_unlock(m);
}

void	print(t_phi *phi, char *str)
{
	lock(&phi->env->write_m);
	if (!someone_dead(phi->env))
		printf("%ld %d %s\n", gettime() - phi->env->t_start, phi->i + 1, str);
	unlock(&phi->env->write_m);
}

void	philo_sleep(int ms)
{
	long	start;

	start = gettime();
	while (gettime() - start < ms)
		usleep(250);
}
