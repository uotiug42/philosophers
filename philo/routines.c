/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routines.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 15:16:30 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/15 10:49:32 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	philo_eat(t_phi *phi)
{
	lock(phi->r_fork);
	print(phi, "has taken a fork");
	lock(phi->l_fork);
	print(phi, "has taken a fork");
	print(phi, "is eating");
	lock(&phi->eat_m);
	phi->l_eat = gettime();
	unlock(&phi->eat_m);
	philo_sleep(phi->env->t_eat);
	lock(&phi->eat_m);
	phi->nb_eat++;
	unlock(&phi->eat_m);
}

void	only_one_philo(t_phi *phi)
{
	print(phi, "has taken a fork");
	usleep(phi->t_die * 1000);
	print(phi, "died");
	lock(&phi->env->dead_m);
	phi->env->dead = 1;
	unlock(&phi->env->dead_m);
}

void	*routine(void *ptr)
{
	t_phi	*phi;

	phi = (t_phi *)ptr;
	print(phi, "is thinking");
	philo_sleep(phi->i % 2 * phi->t_eat / 4);
	if (phi->env->nb_philo == 1)
	{
		only_one_philo(phi);
		return (NULL);
	}
	while (!someone_dead(phi->env))
	{
		philo_eat(phi);
		unlock(phi->r_fork);
		unlock(phi->l_fork);
		print(phi, "is sleeping");
		philo_sleep(phi->env->t_sleep);
		print(phi, "is thinking");
		if (phi->nb_philo % 2 == 1 && phi->t_sleep <= phi->t_eat)
			philo_sleep(phi->t_eat);
	}
	return (NULL);
}
