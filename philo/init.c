/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 15:15:37 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/12 15:15:45 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	init_philo_forks(t_env *e, int i)
{
	if (i + 1 == e->nb_philo)
	{
		e->phi[i].l_fork = &e->fork[(i + 1) % e->nb_philo];
		e->phi[i].r_fork = &e->fork[i];
	}
	else
	{
		e->phi[i].l_fork = &e->fork[i];
		e->phi[i].r_fork = &e->fork[(i + 1) % e->nb_philo];
	}
}

int	init_phi(t_env *e)
{
	int	i;

	i = -1;
	while (++i < e->nb_philo)
	{
		e->phi[i].i = i;
		e->phi[i].l_eat = gettime();
		e->phi[i].nb_eat = 0;
		init_philo_forks(e, i);
		e->phi[i].t_die = e->t_die;
		e->phi[i].t_eat = e->t_eat;
		e->phi[i].t_sleep = e->t_sleep;
		e->phi[i].nb_philo = e->nb_philo;
		e->phi[i].env = e;
		if (pthread_mutex_init(&e->phi[i].eat_m, NULL) != 0)
			return (0);
		if (pthread_create(&e->phi[i].thread, NULL, routine, &e->phi[i]) != 0)
			return (0);
	}
	return (1);
}

int	init_threads(t_env *e)
{
	int	i;

	i = -1;
	while (++i < e->nb_philo)
	{
		if (pthread_mutex_init(&e->fork[i], NULL) != 0)
		{
			while (--i >= 0)
				pthread_mutex_destroy(&e->fork[i]);
			return (ft_free(0, 2, e->phi, e->fork));
		}
	}
	return (1);
}

int	init_env(t_env *e)
{
	e->t_start = gettime();
	e->dead = 0;
	e->phi = malloc(sizeof(t_phi) * e->nb_philo);
	if (!e->phi)
		return (0);
	e->fork = malloc(sizeof(pthread_mutex_t) * e->nb_philo);
	if (!e->fork)
		return (ft_free(0, 1, e->phi));
	if (pthread_mutex_init(&e->write_m, NULL) != 0)
		return (ft_free(0, 2, e->phi, e->fork));
	if (pthread_mutex_init(&e->dead_m, NULL) != 0)
		return (ft_free(0, 2, e->phi, e->fork));
	if (!init_threads(e))
		return (0);
	init_phi(e);
	return (1);
}
