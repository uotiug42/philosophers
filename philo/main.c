/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/09 10:16:30 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/15 11:03:41 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_free(int return_val, int nb_free, ...)
{
	va_list	ap;
	void	*ptr;
	int		i;

	va_start(ap, nb_free);
	i = 0;
	while (i++ < nb_free)
	{
		ptr = va_arg(ap, void *);
		free(ptr);
	}
	return (return_val);
}

int	decode_args(int ac, char **av, t_env *e)
{
	if (!(is_number(av[1]) && is_number(av[2]) && is_number(av[3])
			&& is_number(av[4])))
		return (0);
	if (s_atoi(av[1]) < 1)
		return (0);
	e->nb_philo = s_atoi(av[1]);
	if (s_atoi(av[2]) < 0)
		return (0);
	e->t_die = s_atoi(av[2]);
	if (s_atoi(av[3]) < 0)
		return (0);
	e->t_eat = s_atoi(av[3]);
	if (s_atoi(av[4]) < 0)
		return (0);
	e->t_sleep = s_atoi(av[4]);
	e->max_eat = -1;
	if (ac == 5)
		return (1);
	if (!is_number(av[5]) || s_atoi(av[5]) < 0)
		return (0);
	e->max_eat = s_atoi(av[5]);
	return (1);
}

void	destroy_mutex(t_env *e)
{
	int	i;

	pthread_mutex_destroy(&e->write_m);
	pthread_mutex_destroy(&e->dead_m);
	i = -1;
	while (++i < e->nb_philo)
		pthread_mutex_destroy(&e->fork[i]);
}

int	main(int ac, char **av)
{
	t_env	env;
	int		i;

	if (ac < 5 || ac > 6)
		return (error_args());
	if (!decode_args(ac, av, &env))
		return (error_args());
	if (env.max_eat == 0)
		return (0);
	if (!init_env(&env))
		return (error_threads());
	while (env.nb_philo != 1)
	{
		if (check_death_and_meal(&env))
			break ;
		usleep(250);
	}
	i = -1;
	while (++i < env.nb_philo)
		pthread_join(env.phi[i].thread, NULL);
	destroy_mutex(&env);
	ft_free(0, 2, env.phi, env.fork);
	return (0);
}
