/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/15 11:03:51 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/15 11:04:14 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	error_args(void)
{
	write(2, "Error.\nTo use this program, type :\n", 35);
	write(2, "./philo <number_of_philosopher> <time_to_die> <time_to_eat>", 60);
	write(2, " <time_to_sleep> <(optionnal) number_of_times_each_philo", 55);
	write(2, "sopher_must_eat>\nAll this arguments need to be positive ", 55);
	write(2, "int.\n<number_of_philosopher> must be > 0.\n", 41);
	return (1);
}

int	error_threads(void)
{
	write(2, "Error.\nSomething went wrong initialising the threads...\n", 56);
	return (2);
}
