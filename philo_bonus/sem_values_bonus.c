/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sem_values_bonus.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 16:52:26 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/12 16:52:36 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

int	get_dead_value(t_env *env)
{
	int	dead;

	sem_getvalue(env->dead_sem, &dead);
	return (dead == env->nb_philo);
}

int	get_start_value(t_env *env)
{
	int	start;

	sem_getvalue(env->start_sem, &start);
	return (start);
}

int	get_eat_value(t_env *env)
{
	int	eat;

	sem_getvalue(env->ate_all_sem, &eat);
	return (eat);
}

int	get_forks_value(t_env *env)
{
	int	forks;

	sem_getvalue(env->forks_sem, &forks);
	return (forks);
}
