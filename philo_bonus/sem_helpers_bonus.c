/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sem_helpers_bonus.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 16:54:47 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/12 16:55:19 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

int	create_sem(char *str, sem_t **sem, int val)
{
	sem_unlink(str);
	*sem = sem_open(str, O_CREAT, S_IRUSR, val);
	if (*sem == SEM_FAILED)
		return (0);
	return (1);
}

void	delete_sem(char *str, sem_t **sem)
{
	sem_close(*sem);
	sem_unlink(str);
}

int	delete_sems(int i, t_env *e)
{
	if (i < 4)
		delete_sem("/forks_", &e->forks_sem);
	if (i < 3)
		delete_sem("/write_", &e->write_sem);
	if (i < 2)
		delete_sem("/dead_", &e->dead_sem);
	if (i < 1)
		delete_sem("/ate_all_", &e->ate_all_sem);
	return (0);
}

void	set_eat_sem_name(t_phi *phi, int i)
{
	int	j;

	memset(phi->sem_name, 0, sizeof(char) * 16);
	phi->sem_name[0] = '/';
	phi->sem_name[1] = 'e';
	phi->sem_name[2] = 'a';
	phi->sem_name[3] = 't';
	j = 4;
	while (i)
	{
		phi->sem_name[j] = '0' + i % 10;
		i /= 10;
		j++;
	}
}
