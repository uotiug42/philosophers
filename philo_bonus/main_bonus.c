/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/11 06:40:03 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/15 10:59:02 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "philo_bonus.h"

int	error_args(void)
{
	write(2, "Error.\nTo use this program, type :\n", 35);
	write(2, "./philo <number_of_philosopher> <time_to_die> <time_to_eat>", 60);
	write(2, " <time_to_sleep> <(optionnal) number_of_times_each_philo", 55);
	write(2, "sopher_must_eat>\nAll this arguments need to be positive ", 55);
	write(2, "int.\n<number_of_philosopher> must be > 0.\n", 41);
	return (1);
}

int	error_threads(void)
{
	write(2, "Error.\nSomething went wrong initialising the threads...\n", 56);
	return (2);
}

int	decode_args(int ac, char **av, t_env *e)
{
	if (!(is_number(av[1]) && is_number(av[2])
			&& is_number(av[3]) && is_number(av[4])))
		return (0);
	if (s_atoi(av[1]) <= 0)
		return (0);
	e->nb_philo = s_atoi(av[1]);
	if (s_atoi(av[2]) < 1)
		return (0);
	e->t_die = s_atoi(av[2]);
	if (s_atoi(av[3]) < 0)
		return (0);
	e->t_eat = s_atoi(av[3]);
	if (s_atoi(av[4]) < 0)
		return (0);
	e->t_sleep = s_atoi(av[4]);
	e->max_eat = -1;
	if (ac == 5)
		return (1);
	if (!is_number(av[5]) || s_atoi(av[5]) < 0)
		return (0);
	e->max_eat = s_atoi(av[5]);
	return (1);
}

int	clean_exit(t_env *e, int return_val)
{
	ft_free(0, 1, e->phi);
	sem_close(e->forks_sem);
	sem_unlink("/forks_");
	sem_close(e->write_sem);
	sem_unlink("/write_");
	sem_close(e->dead_sem);
	sem_unlink("/dead_");
	sem_close(e->ate_all_sem);
	sem_unlink("/ate_all_");
	sem_close(e->start_sem);
	sem_unlink("/start_");
	return (return_val);
}

int	main(int ac, char **av)
{
	t_env	env;
	int		i;

	if (ac < 5 || ac > 6)
		return (error_args());
	if (!decode_args(ac, av, &env))
		return (error_args());
	if (env.max_eat == 0)
		return (0);
	if (env.nb_philo == 1)
		return (only_one_philo(&env));
	else if (!init_env(&env))
		return (error_threads());
	env.t_start = gettime();
	if (!init_philo(&env))
		return (clean_exit(&env, 1));
	i = -1;
	while (++i < env.nb_philo)
		waitpid(-1, NULL, 0);
	if (env.nb_philo == 1)
		return (0);
	return (clean_exit(&env, 0));
}
