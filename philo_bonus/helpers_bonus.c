/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 16:54:02 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/12 16:56:59 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

int	ft_free(int return_val, int nb_free, ...)
{
	va_list	ap;
	void	*ptr;
	int		i;

	va_start(ap, nb_free);
	i = 0;
	while (i++ < nb_free)
	{
		ptr = va_arg(ap, void *);
		free(ptr);
	}
	return (return_val);
}

long	gettime(void)
{
	struct timeval	now;
	long			ms;

	gettimeofday(&now, NULL);
	ms = now.tv_sec * 1000 + now.tv_usec / 1000;
	return (ms);
}

void	print(t_phi *phi, char *str)
{
	sem_wait(phi->env->write_sem);
	if (get_dead_value(phi->env) && get_eat_value(phi->env))
		printf("%ld %d %s\n", gettime() - phi->env->t_start, phi->i + 1, str);
	sem_post(phi->env->write_sem);
}

void	philo_sleep(int ms, t_env *e)
{
	long	start;

	start = gettime();
	while (gettime() - start < ms && get_dead_value(e) && get_eat_value(e))
		usleep(250);
}
