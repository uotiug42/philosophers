/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 16:51:03 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/12 16:51:18 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

int	init_sem(t_env *e)
{
	if (!create_sem("/forks_", &e->forks_sem, e->nb_philo))
		return (0);
	if (!create_sem("/write_", &e->write_sem, 1))
		return (delete_sems(3, e));
	if (!create_sem("/dead_", &e->dead_sem, e->nb_philo))
		return (delete_sems(2, e));
	if (!create_sem("/ate_all_", &e->ate_all_sem, e->nb_philo))
		return (delete_sems(1, e));
	if (!create_sem("/start_", &e->start_sem, 1))
		return (delete_sems(0, e));
	return (1);
}

int	init_phi_values(t_phi *phi, int i, t_env *e)
{
	phi->i = i;
	phi->l_eat = gettime();
	phi->nb_eat = 0;
	phi->max_eat = e->max_eat;
	phi->t_die = e->t_die;
	phi->t_eat = e->t_eat;
	phi->t_sleep = e->t_sleep;
	phi->nb_philo = e->nb_philo;
	phi->env = e;
	set_eat_sem_name(phi, i);
	if (!create_sem(phi->sem_name, &phi->eat_sem, 1))
		return (0);
	return (1);
}

int	init_philo(t_env *e)
{
	pid_t	pid;
	int		i;

	i = -1;
	sem_wait(e->start_sem);
	while (++i < e->nb_philo)
	{
		pid = fork();
		if (pid == -1)
		{
			error_threads();
			return (0);
		}
		else if (pid == 0)
			return (philosophers(e, i));
	}
	sem_post(e->start_sem);
	return (1);
}

int	init_env(t_env *e)
{
	e->phi = malloc(sizeof(t_phi) * e->nb_philo);
	if (!e->phi)
		return (0);
	if (!init_sem(e))
		return (ft_free(0, 1, e->phi));
	return (1);
}
