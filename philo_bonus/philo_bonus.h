/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_bonus.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 12:05:36 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/14 11:31:26 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_BONUS_H
# define PHILO_BONUS_H

# include <sys/time.h>
# include <sys/wait.h>
# include <semaphore.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# include <pthread.h>
# include <limits.h>
# include <stdlib.h>
# include <string.h>
# include <stdarg.h>

typedef struct s_phi
{
	pthread_t		checker;
	int				i;
	long			l_eat;
	char			sem_name[16];
	int				nb_eat;
	int				max_eat;
	int				t_die;
	int				t_eat;
	int				t_sleep;
	int				nb_philo;
	struct s_env	*env;
	sem_t			*eat_sem;
}	t_phi;

typedef struct s_env
{
	long			t_start;
	int				nb_philo;
	int				t_die;
	int				t_eat;
	int				t_sleep;
	int				max_eat;
	t_phi			*phi;
	sem_t			*forks_sem;
	sem_t			*write_sem;
	sem_t			*dead_sem;
	sem_t			*start_sem;
	sem_t			*ate_all_sem;
}	t_env;

int		min(int a, int b);
int		max(int a, int b);
int		s_atoi(char *str);
int		init_env(t_env *e);
int		error_threads(void);
int		is_number(char *str);
int		init_philo(t_env *e);
int		only_one_philo(t_env *e);
int		get_eat_value(t_env *env);
int		get_dead_value(t_env *env);
int		get_start_value(t_env *env);
int		get_forks_value(t_env *env);
int		delete_sems(int i, t_env *e);
int		philosophers(t_env *e, int i);
int		ft_free(int return_val, int nb_free, ...);
int		create_sem(char *str, sem_t **sem, int val);
int		init_phi_values(t_phi *phi, int i, t_env *e);

long	gettime(void);

void	print(t_phi *phi, char *str);
void	philo_sleep(int ms, t_env *e);
void	delete_sem(char *str, sem_t **sem);
void	set_eat_sem_name(t_phi *phi, int i);

#endif
