/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routines_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/12 16:56:12 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/15 10:55:20 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

void	*check_routine(void *ptr)
{
	t_phi	*phi;
	long	l_eat;

	phi = (t_phi *)ptr;
	while (get_dead_value(phi->env) && get_eat_value(phi->env))
	{
		usleep(50);
		sem_wait(phi->eat_sem);
		l_eat = phi->l_eat;
		sem_post(phi->eat_sem);
		if (gettime() - l_eat > phi->t_die && get_dead_value(phi->env))
		{
			sem_wait(phi->env->dead_sem);
			sem_wait(phi->env->write_sem);
			if (get_eat_value(phi->env) != 0)
				printf("%ld %d died\n", gettime() - phi->env->t_start,
					phi->i + 1);
			sem_post(phi->env->write_sem);
		}
	}
	return (NULL);
}

int	only_one_philo(t_env *e)
{
	e->t_start = gettime();
	printf("%ld 1 has taken a fork\n", gettime() - e->t_start);
	usleep(e->t_die * 1000);
	printf("%ld 1 died\n", gettime() - e->t_start);
	return (0);
}

void	philo_eat(t_phi *phi)
{
	sem_wait(phi->env->forks_sem);
	print(phi, "has taken a fork");
	sem_wait(phi->env->forks_sem);
	print(phi, "has taken a fork");
	print(phi, "is eating");
	sem_wait(phi->eat_sem);
	phi->l_eat = gettime();
	sem_post(phi->eat_sem);
	philo_sleep(phi->t_eat, phi->env);
	sem_post(phi->env->forks_sem);
	sem_post(phi->env->forks_sem);
	phi->nb_eat++;
}

void	routine(t_phi *phi)
{
	while (!get_start_value(phi->env))
		usleep(50);
	print(phi, "is thinking");
	if (pthread_create(&phi->checker, NULL, check_routine, phi) != 0)
		return ;
	philo_sleep(phi->i % 2 * phi->t_eat, phi->env);
	while (get_dead_value(phi->env) && get_eat_value(phi->env))
	{
		philo_eat(phi);
		print(phi, "is sleeping");
		if (get_eat_value(phi->env) != 0 && phi->nb_eat == phi->max_eat)
			sem_wait(phi->env->ate_all_sem);
		philo_sleep(phi->t_sleep, phi->env);
		print(phi, "is thinking");
		if (phi->nb_philo % 2 == 1 && phi->t_sleep <= phi->t_eat)
			philo_sleep(phi->t_eat, phi->env);
	}
	pthread_join(phi->checker, NULL);
	delete_sem(phi->sem_name, &phi->eat_sem);
}

int	philosophers(t_env *e, int i)
{
	if (!init_phi_values(&e->phi[i], i, e))
	{
		error_threads();
		return (0);
	}
	routine(&e->phi[i]);
	return (1);
}
